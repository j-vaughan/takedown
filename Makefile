VPATH = src:../headers:build
CC=gcc
CFLAGS=I.

takedown: program.o
	$(CC) -o tbuild_takedown program.o

program.o: program.c
	$(CC) -c build/program.c

clear:
	-rm tbuild*
	-rm program.o

clean:
	-rm program.o