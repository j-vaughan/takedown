#include <stdio.h>

#include "cr_user.h"

int consent () { // Asks user for generic permission 
	int c;

	printf("Continue? y/n: ");
	c = getchar( );

	if (c == 121) {
		return 0;
	}
	else if (c == 110) {
		return 1;
	}
	else return -1;
}