#include <stdio.h>	// For basic functions (like printf)
#include <unistd.h> // To check if files exist
#include <string.h>	// For string manipulation

int consent();				// You should declare your methods before using them, with input types
int print_all(char[]);

struct Token {				// Like a custom C# class with properties
	int		type;
	char	value[81];		// Cannot be assigned with `tokenName.value = source`. Must use `strcpy(tokenName.value, source);`.
};

int consent_in = 0;					// This is used to hold the yes/no input from the user
char input_path[4097];				// Holds path of input file, based on maximum path in Linux, plus null terminator

int main (int argc, char *argv[]) {	// ArgC is the amount of command-line arguments supplied, ArgV[] is the array of those arguments
	int i = 0;						// Unlike C#, you have to declare all variables at once, before using them. Even the for() loop variables
	int type_buff[6];				// This is used to identify the type, based on the maximum recognized keyword length (5), plus null terminator
	FILE *input_file;				// Data type used for reading from/writing to files. * identifies pointer

	if (argc > 1 && access(argv[1], R_OK) == 0) {		// If there is an input file supplied, and it's readable
		printf("Input file readable\n");					// \n is the same as pressing enter after your text: like Console.WriteLine

		consent_in = consent();								// Asks permission to continue

		if (consent_in == 1) {								// If permission was given
			strcpy(input_path, argv[1]);
			
			printf("Opening \'%s\'...\n", input_path);		// %s is used to insert strings into printf, this inserts the user-provided path
			
			// Do stuff

			return 0;
		}
		else if (consent_in == -1) {						// If permission was not given, exit with success code (0)
			printf("Exiting...");
			return 0;
		}
		else return -4;										// Else exit with panic! error (-4)
	}
	else if (argc > 1 && access(argv[1], R_OK) != 0) {	// If there is an input file supplied, but it's not readable
		printf("Error -1: Input file unreadable\n");
		return -1;											// Exit with unreadable_file error (-1)
	}
	else if (argc == 1) {								// If no input file supplied
		printf("Error -2: No input file\n");
		return -2;											// Exit with no_input_file error (-2)
	}
	else {
		printf("Error -4: Panic!\n");
		return -4;										// Else exit with panic! error (-4)
	}

	printf("Error -4: Panic!\n");
	return -4;										// Default exit with panic! error (-4) since this should be unreachable
}

int hello () { // Prints "Hello, world!" 
	printf("Hello, world!\n");
	return 0;
}

int consent () { // Asks user for generic permission 
	int c;

	consent_in = 0; // Reset current value

	printf("Continue? y/n: ");
	c = getchar( );

	if (c == 121) {
		return 1;
	}
	else if (c == 110) {
		return -1;
	}
	else return 0;
}

int print_all (char file_name_in[]) { // Takes input file without checking if it's readable and procedurally outputs contents 
	FILE *print_all_file;
	int char_in;
	int i = 0;

	print_all_file = fopen(file_name_in, "r");
	char_in = fgetc(print_all_file);
	
	while (char_in != EOF) {
		i++;
		printf("%i ", i);

		while (char_in != 10 && char_in != 13) {
			putchar(char_in);
			char_in = fgetc(print_all_file);
		}

		putchar(10);
		char_in = fgetc(print_all_file);
	}

	if (fclose(print_all_file) == 0)
		return 0;
	else return -1;
}
