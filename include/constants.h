#ifndef _CONSTANTS_H_
#define _CONSTANTS_H_

const int LF 	    = 10;	// Line feed
const int CR 	    = 13;	// Carriage return
const int SPACE     = 32;	// Space character
const int HASH	    = 35;	// #
const int STAR      = 42;	// *

const int TEXT      =  0;
const int TITLE     =  1;
const int SUBTITLE  =  2;
const int HEAD1     =  3;
const int HEAD2     =  4;
const int HEAD3     =  5;
const int BOLD      = 10;
const int ITALIC    = 11;

#endif